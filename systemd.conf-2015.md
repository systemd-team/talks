---
title: systemd integration and CI in Debian/Ubuntu
author: Michael Biebl,
        Martin Pitt
date: \today
theme: Singapore
<!-- header-includes: \setbeameroption{show only notes} -->
...

Overview
--------
 - About Debian
 - Short history of systemd in Debian
 - Challenges with integrating systemd into Debian
 - Categorization of patches
 - The Debian release process
 - Automated tests for distro landing and upstream QA
 - Thoughts for improvements and discussion


About Debian
------------
 - truly community based distribution
 - (traditionally) strong package maintainer ←→ package relationship
 - huge software archive
 - many architectures
 - stable release about every 2 years
 - famous for its conservative approach to stable releases
 - base distribution for a lot of derivatives

\note[item]{with 22 year one of the oldest distros }
\note[item]{~ 23500 src packages, almost 50000 binary packages}
\note[item]{Introduce myself as part of Debian}
\note[item]{derivates: Ubuntu, mention Martin}

Short history of systemd in Debian
----------------------------------

 - 2010: first systemd Debian package
 - May 2013: Wheezy release with systemd 44 as an option
 - 2013: increasing pressure from upstreams/admins to switch to systemd
 - Oct 2013: CTTE bug "Which init will be the default?"
 - 2014: The "Debian Init System Wars", CTTE discussion
 - Feb 2014: CTTE decides that the default should be systemd
 - Apr 2015: Jessie released with systemd 215 as default
 - today: New upstream releases land in Debian "the next day"

\note[item]{shows timeline of bringing systemd into Debian}
\note[item]{discussions about modern init system since 2007, increasing pressure from some upstream projects and admins to use features like logind or journal)}

\note[item]{work not done with v215 release in jessie, lots of stuff still to do outside of the systemd package}
\note[item]{next graph shows the development around jessie and how we struggled to get new releases out in a more timely manner}

Keeping up with new releases
----------------------------

![releases](images/days-to-debian.pdf)\


\note[item]{v204: udev merge}
\note[item]{v208: systemd-shim}

Debian integration challenges
-----------------------------

challenges imposed by Debian

 - support for non-Linux kernels → multiple inits
 - smooth upgrade paths, chance to opt out of the switch
 - base system is not maintained by a single team
 - combinatorial explosion of system configurations
 - resistance/indifference of individual maintainers

\note[item]{transitional upgrades are important for Debian}
\note[item]{upgrade instead of fresh installation, accumulated cruft}
\note[item]{cryptsetup}

---

Debian integration challenges
-----------------------------

challenges imposed by upstream

 - ever increasing scope
 - moving fast, lot's of churn
 - picky
 - quick to add external dependencies or requiring latest versions
 - compat support is considered a burden, limited to the minimum
 - only the latest\footnote{or latest two} upstream version is supported


Growth
------

![growth](images/growth.pdf)\


Release frenzy
--------------

![releases](images/days-between-releases.pdf)\



\note[item]{make compromises to work with existing/obsolete stuff (e. g. rcS scripts) -> better to compromise and use it now than postponing the move to far future, write migration guide}
\note[item]{planning the upgrade / multiple init systems -> etwas ausbauen, debhelper etc. "uebersetzen In upstream-verstaendliche Terminologie}
\note[item]{problem: sysvinit in wheezy is an essential package}
\note[item]{support for switching between inits, not just doing a one time switch}
\note[item]{split sysvinit into sysvinit-core/sysvinit to allow upgrade in one release cycle}
\note[item]{complexity caused by supporting multiple init systems}
\note[item]{dh-systemd / i-s-h for compatibility with non-systemd systems}
\note[item]{systemd-shim / sysv compatibility (sysvinit/sysvinit-core) as replacement for ConsoleKit}
\note[item]{syncing the state between sysv / systemd}
\note[item]{migrate configuration where sensible, don't keep using the old config files}
\note[item]{switch to systemd on upgrades for "normal" upgrade paths}
\note[item]{but keeping sysvinit around on upgrades as fallback}


How to tackle this
------------------
 - switch by default but keep a fallback on upgrades
 - wrappers/helpers like dh_systemd which try to hide what init system is used
 - making components of systemd usable on non-systemd systems
 - conservative when enabling new featues, split if necessary
 - getting other maintainers on board, forming a team around systemd
 - getting new Debian releases out quicker
 - reduce diff to upstream (and also to downstream, specifically Ubuntu)


\note[item]{dh_systemd/i-s-h, ugly but necessary to keep additional state}
\note[item]{unsolved: on-demand services during upgrades/deinstallation}
\note[item]{udev and logind for sysvinit}
\note[item]{split to keep the footpring (installed size) down and avoid pulling in to many deps in the core package}
\note[item]{container, journal-remote, maybe boot}
\note[item]{realisation that we want to track new upstream releases faster}
\note[item]{quite obvious, that systemd is too big for a single maintainer, forming a team with m-l, IRC,..}
\note[item]{core packages, like util-linux fine nowadays}

Categorization of patches
-------------------------

\# of patches (in sid)

 - 5 for other init systems
 - 17 for Debianisms (different config files, tmpfiles, etc.)
 - 7 for use cases with no interest from upstream
 - 6 for upstream bugs
 - by far biggest part of daily upstream CI


\note[item]{making components like logind usable for non-systemd}
\note[item]{sysv compat, /run/initctl}
\note[item]{insserv}

\note[item]{different file locations}
\note[item]{console-setup vs vconsole}

\note[item]{fsckd, split-usr}

\note[item]{ upstreaming patches more aggressively (e.g. update-rc.d support)}
\note[item]{ but at the same time patches goes up due to systemd growing and rejecting "real-world compromise" patches (SIGKILL on shutdown, fsck, don't destroy custom cgroups, etc.) or patches for non-/usr/ merge}


The Debian release process
--------------------------
 - unstable → testing → stable
 - maintainers upload to unstable
 - packages migrate (automatically) from unstable to testing
 - testing is frozen before a new release
 - a new stable release is based on what is in testing at that point
 - goal: always releasable testing

\note[item]{testing migration: happens after X (typically 5) days if certain conditions are met: built everywhere, no new RC bugs}
\note[item]{during freeze, no new upstream releases, only (RC) bugs fixes allowed}
\note[item]{no new upstream releases in stable}

QA when packaging new releases
------------------------------

 - lintian, static checks
 - running the builtin test-suite during build
 - maintainer(s) testing the package
 - user bug reports
 - new RC bugs preventing testing migration

\note[item]{lintian: testing policy compliance, but also common packaging mistakes, empty packages etc. Mention dpkg-gensymbols}
\note[item]{lintian: autorejects by ftp-master}
\note[item]{bug reports: preferably from sid users}
\note[item]{don't break sid too much, it's widely used, tools like apt-listbugs help}
\note[item]{how can we make sure that releasing new stuff faster doesn't break users boot (unstable is in daily use for lot's of people)}
\note[item]{how to prevent broken upstream releases}


autopkgtest: Basic idea
-----------------------


- run automated tests against the installed packages in a real OS installation with full machine access
  \footnote{not in Debian yet, only schroot}
- roughly 3000 packages with autopkgtests in Debian/Ubuntu now


\note[item]{Every test gets fresh VM or container or chroot, can do arbitrary changes and reboots}
\note[item]{as opposed to: upstream unit tests running during package build ("make check")}
\note[item]{Detects packaging issues (wrong file installation, missing dependencies) and regressions when your dependencies change}
\note[item]{Slide shows tiny example of one check in the logind test that lid closing triggers suspend}
\note[item]{over time accumulated quite a lot, details later}

autopkgtest: Structure
----------------------
debian/tests/control:

\footnotesize

    Tests: logind
    Depends: libpam-systemd, acl, evemu-tools
    Restrictions: needs-root, isolation-machine

\normalsize
debian/tests/logind:
\footnotesize

    evemu-device $testdir/lidswitch.evemu &
    [...]
    # closing lid should cause instant suspend
    evemu-event $LID_DEV --sync --type 5 --code 0 --value 1
    wait_suspend 2
    evemu-event $LID_DEV --sync --type 5 --code 0 --value 0

\note[item]{Source packages ship integration tests}
\note[item]{actual tests are typically written in sh or python (dbus)}

autopkgtest: Purpose
--------------------

- pre-upload checks
- `git bisect` to find regressions
- involve upstream

\note[item]{once you have these tests: lots of goodness}
\note[item]{Us devs use them to verify before we upload to the distro}
\note[item]{on regression: shell into the testbed, investigate, run parts of the test, copy in binaries with patches out of git build}
\note[item]{greatly helpful for git bisection: motivates us to write tests for reported regressions, then run bisect to identify the upstream commit and discuss upstream/revert}


autopkgtest: Purpose
--------------------

\footnotesize

util-linux (2.26.2-3 to 2.27.1-1)

 - systemd 227-1: amd64 \colorbox{green}{Pass}, armhf: \colorbox{red}{Regression}, i386: \colorbox{green}{Pass}
 - udisks2 2.1.6-1: [...]

\normalsize

\url{http://people.canonical.com/~ubuntu-archive/proposed-migration/update_excuses.html}

\note[item]{second important purpose: gating of uploads into the distro}
\note[item]{Ubuntu (not yet Debian): Developers can't directly upload to dev release, but to "staging" area ("devel-proposed")}
\note[item]{Packages need to build and be installable on all arches, succeed their own tests, and - important - succeed tests of packages that depend on it}
\note[item]{Example: new util-linux version causes a regression of systemd on armhf → held back, devs need to resolve, or can remove the proposed upload again}
\note[item]{As a result: Never land/release regressions which can be detected automatically}

autopkgtest: Purpose
--------------------

\url{https://launchpad.net/~pitti/+archive/ubuntu/systemd}

![daily CI PPA](images/daily_ppa.png)\quad

\note[item]{Finally: we have scripts that apply distro patches and packaging to checkout of upstream master}
\note[item]{construct a source package, build it (runs upstream unit tests), runs autopkgtest on the built packages, uploads to PPA for others to test}
\note[item]{Of course the tests can never cover everything, but they should be good enough to ensure that we don't break booting for large number of people}
\note[item]{gives us "daily builds with some confidence", and finds a surprising number of regressions that landed in trunk a day or three ago}
\note[item]{autopkgtest do not happen automatically for PPAs}

Debian's systemd integration tests
----------------------------------
 - timedated, hostnamed, localed-locale, localed-x11-keymap
 - logind: user session, lid suspend, inhibition, shutdown, hot/coldplug device
   ACLs
 - bootchart
 - unit-config: enable/disable unit, SysV init, combinations, `Alias=`,
   `systemd-sysv-install`
 - networkd: hot/coldplug IPv4/6, `n-wait-online`, expected `networkctl`

\note[item]{categories: crucial for stability, reproducer of bug reports/regressions, areas seldomly tested manually, areas patched downstream}
\note[item]{timedated and similar: call timedatectl, check config files before/after and no crashes of daemon}
\note[item]{important as we have some heavy patches there due to different Debian config files}
\note[item]{logind: detected crash with setting wall messages (took ~ 3 upstream commits to get it right) and lid suspend not working in the first 3 minutes after boot}
\note[item]{bootchart: boot with init=systemd-bootchart, assert .svg is present}
\note[item]{unit-config: partially covered in upstream unit tests, but SysV integration is distro specific; found bugs with Alias= and SysV etc.}
\note[item]{networkd: test with a veth; already found at least three upstream regressions in trunk: \#1147, \#1368, \#1645}

Debian's systemd integration tests 2
------------------------------------
 - build-login: build trivial `.c` against `libsystemd-dev`
 - boot-and-services: no failed units, D-Bus, NetworkManager, lightdm, cron, logind, rsyslog, udev, `/tmp` cleanup, nspawn, AppArmor, custom cgroups, seccomp
 - ifupdown-hotplug
 - cmdline-upstart-boot
 - systemd-fsckd
 - boot-smoke

\note[item]{build-login: tests correct libs and install of pkg-config, sufficient dependencies of -dev package}
\note[item]{boot-and-services is a grab-bag: clean boot with newly installed version, most important services start; found regressions in `tmpfiles` and nspawn, systemd messing up custom cgroups of other services}
\note[item]{ifupdown-hotplug: Debian specific bringup of network interfaces}
\note[item]{cmdline-upstart-boot: system and most important services still boot with upstart}
\note[item]{systemd-fsck: plymouth integration of (multiple) fscks; removed upstream, but still relevant for existing systems}
\note[item]{boot-smoke: reboot 20 times, check for polkit and timeouts; found issues like the recent \#1505}

Possible improvements and discussion
------------------------------------

 - frequent releases and semaphore: good!
 - better discipline before releasing!
 - declare some releases as LTS?
 - QA/stable release updates in other distros?
 - do other distros split up systemd?

\note[item]{quite happy with current release cycles and quality of releases}
\note[item]{when announcing new releases, stick to the date and stop landing non-bug fix PRs, refactoring etc.}
\note[item]{idea: upstream LTS releases (distro maintained) which multiple distros using the same version; needs alignment of releases, but maybe more practical replacement of old stable branches?}

Resources
----------

 - IRC: `#debian-systemd` on OFTC (mbiebl, pitti)
 - mailing list:\
   \url{http://lists.alioth.debian.org/mailman/listinfo/pkg-systemd-maintainers}
 - Debian package:\
   \url{https://tracker.debian.org/pkg/systemd}
 - autopkgtest:\
   \url{http://ci.debian.net/} \
   \url{http://autopkgtest.ubuntu.com/}
<!---
vim: ft=markdown
build: pandoc -t beamer -o systemd.conf-2015.pdf systemd.conf-2015.md
-->
