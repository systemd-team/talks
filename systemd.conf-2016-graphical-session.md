---
title: graphical-session.target
subtitle: Using systemd to run X11/Wayland/Mir sessions
author: Martin Pitt
date: systemd.conf 2016
theme: Singapore
<!-- header-includes: \setbeameroption{show notes} -->
<!-- for dspdfviewer: -->
header-includes: \usepackage{pgfpages}\setbeameroption{show notes on second screen}
...

Motivation
----------
Colin Walters, 2013:

> \url{https://wiki.gnome.org/ThreePointThirteen/Features/SystemdUserSession}


\vfill

Olav Vitters, 2014:

>  "In the future I see systemd user sessions more or less replacing gnome-session."

\vfill

upstart → systemd on Ubuntu

\note[item]{systemd: always intended as as session manager}
\note[item]{Colin: experiments and patches, Olav Vitters blog, then it silently died → clear demand}
\note[item]{Debian/Ubuntu: systemd transition done on system side; upstart for session (unmaintained, not two techs)}
\note[item]{going back to gnome-session/D-Bus activation not an option:
auto-restart, restart limit, hotplug/dynamic services, need ordering/dependencies}
\note[item]{understand and think about architecture and conceptual/practical difficulties}

Graphical session today
-----------------------
/usr/share/xsessions/srd.desktop:
```ini
  [Desktop Entry]
  Name=Some Random Desktop
  Exec=/usr/bin/srd-session
```

\note[item]{DMs show all available *.desktops}
\note[item]{Exec= is session root and process leader: stop → session end (gnome-session)}

X11 session today
-----------------
/etc/X11/Xsession.d/10ssh-agent:
```sh
  STARTUP="$SSHAGENT $STARTUP"
```

\vfill

/etc/X11/Xsession.d/90gpg-agent:
```sh
  eval `gpg-agent --sh`
  export GPG_AGENT_INFO
```

\note[item]{stuff that needs to happen early for every session}
\note[item]{STARTUP is from Exec=, hooks prepend wrapper scripts like dbus-launch}
\note[item]{poor man's service ordering}
\note[item]{n/a for Mir/Wayland}

Modelling with systemd --user
-----------------------------
srd-session.target:
```ini
  BindsTo=srd-session.service
  Requires=srdwm.service srd-settings-daemon.service
```

srd-session.service:
```ini
  PartOf=srd-session.target
  ExecStart=/usr/bin/srd-session
```

srdwm.service:
```ini
  PartOf=srd-session.target
  After=srd-session.service
  ExecStart=/usr/bin/srdwm
  Restart=on-failure
```

\note[item]{Let's just naïvely try and see what happens}
\note[item]{xsessions desktop → corresponding target, for dependencies; components as services}
\note[item]{most of this should not surprise anyone here; usual fanciness}
\note[item]{BindsTo: session leader, PartOf: go down with the target}
\note[item]{Works in principle, but several problems}

Running the session
-------------------
/usr/share/xsessions/srd.desktop:
```ini
  [Desktop Entry]
  Name=Some Random Desktop
  Exec=/usr/bin/srd-session
```

\note[item]{How do we start the session -- everything is in units now; replace Exec}

Running the session
-------------------

/usr/share/xsessions/srd.desktop:
```ini
  [Desktop Entry]
  Name=Some Random Desktop
  Exec=/usr/bin/systemctl start srd-session.target
```

\note[item]{naïvely, just start our target → do you see the problems?}
\note[item]{session leader, session goes down when Exec= finishes}

Running the session -- ideal
----------------------------
/usr/share/xsessions/srd.desktop:
```ini
  [Desktop Entry]
  Name=Some Random Desktop
  Exec=/usr/bin/systemctl start --wait srd-session.target
```

\note[item]{need to block until target stops again → PR pending}
\note[item]{completely voids Xsession.d wrappers as they just wrap systemctl → early units with appropriate dependencies}
\note[item]{this is ideally how Exec= should look like}

Running the session -- ugly real world
--------------------------------------
\tiny

```sh
#!/bin/sh -e
# some old Qt programs still check this long-deprecated env var
dbus-update-activation-environment --systemd GNOME_DESKTOP_SESSION_ID=this-is-deprecated

# robustness: if the previous graphical session left some failed units,
# reset them so that they don't break this startup
for unit in $(systemctl --user --no-legend --state=failed list-units | cut -f1 -d' '); do
    if [ "$(systemctl --user show -p PartOf --value)" = "graphical-session.target" ]; then
        systemctl --user reset-failed $unit
    fi
done

systemctl --user restart graphical-session-pre.target
systemctl --user restart "$1"

# Wait until the session gets closed
# FIXME: replace with an event-based waiting instead of polling (needs new systemctl functionality)
while systemctl --user --quiet is-active "$1"; do sleep 1; done

dbus-update-activation-environment --systemd GNOME_DESKTOP_SESSION_ID=

# Delay killing the X server until all graphical units stopped
# FIXME: we currently cannot make targets wait on its dependencies going to
# "inactive", only to "deactivating"
while [ -n "$(systemctl --user --no-legend --state=deactivating list-units)" ]; do sleep 0.2; done
```

\note[item]{actual Exec= startup script in Ubuntu devel series}
\note[item]{don't expect you to read/understand, just appreciate/sigh on size and shell polling loops}
\note[item]{is-active pool loop: --wait from above}
\note[item]{consider as prototype, still some work to do until our goal}

Using services in different session types
-----------------------------------------
nautilus.service:
```ini
  PartOf=gnome-session.target
```

\note[item]{stop service together with the session}
\note[item]{realize that some services are being used in more than one session}
\note[item]{gnome-session itself, multiple flavors of LXDE}

Using services in different session types
-----------------------------------------
nautilus.service:
```ini
  PartOf=gnome-session.target unity-session.target
  PartOf=mate-session.target
```

\note[item]{every new session → touch all existing units}
\note[item]{really want to say "any graphical session"}

Using services in different session types
-----------------------------------------
nautilus.service:
```ini
  PartOf=graphical-session.target
```

\{srd,gnome,mate,…\}-session.target:
```ini
  BindsTo=graphical-session.target
```

\note[item]{g-s.t acts as "alias" for any graphical session}
\note[item]{every concrete session starts/stops that along with itself}
\note[item]{in systemd 231; target is trivial (just a name), but important convention}

Ordering early services
-----------------------
graphical-session-pre.target:
```ini
  Wants=gpg-agent.service ssh-agent.service
  Wants=gnome-keyring.service
  Before=graphical-session.target
```

\note[item]{create units for Xsession.d scripts, dbus-update-activation-environment}
\note[item]{before "actual" session services; not how systemd units work}
\note[item]{delays the target itself, but none of it's dependencies; need to add After=graphical-session-pre.target to every service}
\note[item]{inconvenient/counter-intuitive}
\note[item]{system side: DefaultDependencies=yes does that After=sysinit.target}

Ordering early services
-----------------------
```sh
  systemctl --user start graphical-session-pre.target
  systemctl --user start "$1"
```

\vfill

\url{https://github.com/systemd/systemd/issues/3750}

\note[item]{workaround: first start -pre, then the session}
\note[item]{generalization of DefaultDependencies? change target semantics? live with workaround? change all units?}
\note[item]{appreciate discussion}

User bus/services model
-----------------------
\scriptsize

```sh
[...]
systemctl --user start srd-session.target
[...]

# Delay killing the X server until all graphical units stopped
# FIXME: we currently cannot make targets wait on its dependencies going to
# "inactive", only to "deactivating"
while [ -n "$(systemctl --user --no-legend --state=deactivating list-units)" ]
do
    sleep 0.2
done
```

\note[item]{systemd user unit for all sessions, implies moving to user-centric D-Bus as well}
\note[item]{recently enabled by default in Ubuntu, opt-in in Debian, Fedora unknown}
\note[item]{supposedly not yet wide-spread: bugs in polkit, gnome-keyring, gnome-terminal (fixed upstream)}
\note[item]{stopping session services on graphical logout}
\note[item]{stopping target -> units deactivate, not "stopped"}
\note[item]{kill X too early, remaining running services will start failing, units auto-restart}
\note[item]{would need semantics of "stop target when all dependencies are stopped"}

User bus/services model
-----------------------
\scriptsize

```sh
# robustness: if the previous graphical session left some
# failed units, reset them so that they don't break startup
for unit in $(systemctl --user --no-legend --state=failed list-units |
              cut -f1 -d' '); do
    if [ "$(systemctl --user show -p PartOf --value)" =
             "graphical-session.target" ]; then
        systemctl --user reset-failed $unit
    fi
done

[...]
systemctl --user start srd-session.target
[...]
```

\note[item]{same vein -- reset failed units related to graphical session before logging in}
\note[item]{have not thought about how to do that more cleanly; input appreciated}

Conclusion
----------
\qquad ![](images/airport-BER.jpg){width=100%}

\note[item]{systemd session sort of works, but is not polished yet, and small technical issue might lead to major malfunction}
\note[item]{need to provide some missing functionality to make it elegant and robust without hacks, then wider adoption to other session types and upstreaming}
\note[item]{until then, limited to Unity session and (limited) Xubuntu flavor}
