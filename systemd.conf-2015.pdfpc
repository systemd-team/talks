[file]
systemd.conf-2015.pdf
[end_user_slide]
22
[notes]
### 1
- introduce myself as DD and core member of pkg-systemd
- mention Martion Pitt as co-presenter
- thanks to organizers

### 2
- overview
### 3
- with 22 year one of the oldest distros
- some DDs younger, some members go back almost to the beginning
- ~ 23500 src packages, almost 50000 binary packages
- release when ready: ~ 2 years
- derivates: Ubuntu, Martin

### 4
- talks about a modern init system date back to at least 2007
- ubuntu switched (immediately) as a result
- after all the controversy, the actual release was mostly uneventful
- next graph shows development
- struggle to get out new releaeses

### 5
- v44: version in wheezy
- v204: udev merge
- v208: systemd-shim needed to be developed
- v215: version in jessie
- will be interesting to see if we run into issues like that in the future
  again

### 6
- switch would probably not have been accepted if sysvinit was dropped
- transitional upgrades are important for Debian
- upgrade instead of fresh installation, accumulated cruft
- still quite a bit of controversy, but it has died down significantly
- indifference: especially a problem when adding systemd support to packages

### 7
- growth: switch to graph on next slide
- moving fast: switch to graph on slide +2
- correctness (mtab, fstab)
- full use of existing APIs with no real graceful fallback

### 8
graph growth
- scope inscrease
- configuration directives / testing surface

### 9
graph release frenzy

- brown paper bag releases
- unannounced, makes it harder to plan for
- high frequency, not really regular (yet)
- v221 switched to github

### 10
- dh_systemd/i-s-h, ugly but necessary to keep additional state
- unsolved: on-demand services during upgrades/deinstallation
- udev and logind for sysvinit
- split to keep the footprint down and avoid pulling in to many deps in the core package
- container, journal-remote, maybe boot}
- too big for a single maintainer, forming a team with m-l, IRC,..
- realisation that we want to track new upstream releases faster
- core packages, like util-linux fine nowadays



### 11
- making components like logind usable for non-systemd
- sysv compat, /run/initctl
- insserv

- different file locations
- console-setup vs vconsole

- fsckd, split-usr

- upstreaming patches more aggressively (e.g. update-rc.d support)
- but at the same time patches goes up due to systemd growing and rejecting "real-world compromise" patches (SIGKILL on shutdown, fsck, don't destroy custom cgroups, etc.)


### 12
- testing migration: happens after X (typically 5) days if certain conditions are met: built everywhere, no new RC bugs
- during freeze, no new upstream releases, only (RC) bugs fixes allowed
- no new upstream releases in stable


### 13
- lintian: testing policy compliance, but also common packaging mistakes, empty packages
- lintian: autorejects by ftp-master
- bug reports: preferably from sid users
- don't break sid too much, it's widely used
- how can we make sure that releasing new stuff faster doesn't break users boot - even better: how to prevent broken upstream releases


### 14
- Every test gets fresh VM or container or chroot, can do arbitrary changes and reboots
- as opposed to: upstream unit tests running during package build ("make check")
- Detects packaging issues (wrong file installation, missing dependencies) and regressions when your dependencies change
- Slide shows tiny example of one check in the logind test that lid closing triggers suspend
- over time accumulated quite a lot, details later

### 15
- Source packages ship integration tests
- actual tests are typically written in sh or python (dbus)

### 16
- once you have these tests: lots of goodness
- Us devs use them to verify before we upload to the distro
- on regression: shell into the testbed, investigate, run parts of the test, copy in binaries with patches out of git build
- greatly helpful for git bisection: motivates us to write tests for reported regressions, then run bisect to identify the upstream commit and discuss upstream/revert

### 17
- second important purpose: gating of uploads into the distro
- Ubuntu (not yet Debian): Developers can't directly upload to dev release, but to "staging" area ("devel-proposed")}
- Packages need to build and be installable on all arches, succeed their own tests, and - important - succeed tests of packages that depend on it
- Example: new util-linux version causes a regression of systemd on armhf → held back, devs need to resolve, or can remove the proposed upload again
- As a result: Never land/release regressions which can be detected automatically


### 18
- Finally: we have scripts that apply distro patches and packaging to checkout of upstream master
- construct a source package, build it (runs upstream unit tests), runs autopkgtest on the built packages, uploads to PPA for others to test
- Of course the tests can never cover everything, but they should be good enough to ensure that we don't break booting for large number of people
- gives us "daily builds with some confidence", and finds a surprising number of regressions that landed in trunk a day or three ago
- autopkgtest do not happen automatically for PPAs

### 19
- categories: crucial for stability, reproducer of bug reports/regressions, areas seldomly tested manually, areas patched downstream
- timedated and similar: call timedatectl, check config files before/after and no crashes of daemon
- important as we have some heavy patches there due to different Debian config files
- logind: detected crash with setting wall messages (took ~ 3 upstream commits to get it right) and lid suspend not working in the first 3 minutes after boot
- bootchart: boot with init=systemd-bootchart, assert .svg is present
- unit-config: partially covered in upstream unit tests, but SysV integration is distro specific; found bugs with Alias= and SysV etc.
- networkd: test with a veth; already found at least three upstream regressions in trunk: \#1147, \#1368, \#1645

### 20
- build-login: tests correct libs and install of pkg-config, sufficient dependencies of -dev package
- boot-and-services is a grab-bag: clean boot with newly installed version, most important services start; found regressions in `tmpfiles` and nspawn, systemd messing up custom cgroups of other services
-i fupdown-hotplug: Debian specific bringup of network interfaces
- cmdline-upstart-boot: system and most important services still boot with upstart
- systemd-fsck: plymouth integration of (multiple) fscks; removed upstream, but still relevant for existing systems
- boot-smoke: reboot 20 times, check for polkit and timeouts; found issues like the recent \#1505


### 21
- quite happy with current release cycles and quality of releases
- when announcing new releases, stick to the date and stop landing non-bug fix PRs, refactoring etc.
- hookup autopkgtest upstream
- LTS release every year or so with more support from upstream
- how do other distros decide which upstream release they pick
- how do they handle the inscreasing scope?































