#!/bin/sh

#set -x

echo "upstream, debian_revision, release date upstream, unstable, experimental,"
for i in `seq 1 44` `seq 183 227`; do
	upstream=$(grep "v$i," ~/dates.csv | cut -f2 -d',' | cut -f1 -d',')
	distro=$(dpkg-parsechangelog -f $i-1 -t $i-1 -S Distribution 2>/dev/null)
	full_date=$(dpkg-parsechangelog -f $i-1 -t $i-1 -S Date 2>/dev/null)
	short_date=$(date --date="$full_date" +"%Y-%m-%d")
	if [ -n "$distro" ] && [ "$distro" = unstable  ] ; then
		echo "v$i, $i-1, $upstream, $short_date,,"
	elif [ -n "$distro" ] && [ "$distro" = experimental  ] ; then
		echo "v$i, $i-1, $upstream, ,$short_date,"
		# find the first unstable upload
		j=$(($i+1))
		max_rev=$(dpkg-parsechangelog -f $i -t $j -S Version 2>/dev/null | cut -f2 -d'-')
		for v in `seq 1 $max_rev` ; do
			distro=$(dpkg-parsechangelog -f $i-$v -t $i-$v -S Distribution 2>/dev/null)
			if [ -n "$distro" ] && [ "$distro" = unstable  ] ; then
				full_date=$(dpkg-parsechangelog -f $i-$v -t $i-$v -S Date 2>/dev/null)
				short_date=$(date --date="$full_date" +"%Y-%m-%d")
				echo "v$i, $i-$v, $upstream, $short_date,,"
				break
			fi
		done
	fi
	
done
