#!/bin/sh

#set -e

tmp=$(mktemp)

echo "version, datum," >> $tmp

for i in `seq 1 44` `seq 183 227`; do
	v="v${i}"
	full_date=$(git show -s --format=%ci $v | tail -n1)
	short_date=$(date --date="$full_date" +"%Y-%m-%d")
	if [ -n "$short_date" ] ; then
		echo "${v}, $short_date," >> $tmp
	fi
done

mv $tmp ~/dates.csv
