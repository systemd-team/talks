#!/bin/sh

#set -e

tmp=$(mktemp)

echo "version, entities, sloc, datum," >> $tmp

for i in `seq 190 227`; do
	v="v${i}"
	echo "$v"
	git clean -xdf >/dev/null
	git checkout "$v" >/dev/null
	sloc=$(sloccount . 2>/dev/null | grep "Total Physical Source Lines of Code" | cut -f2 -d'=' | sed s/,//)
	./autogen.sh c >/dev/null
	make man/systemd.directives.xml >/dev/null
	num=$(grep varlistentry man/systemd.directives.xml | wc -l)
	full_date=$(git show -s --format=%ci $v | tail -n1)
	short_date=$(date --date="$full_date" +"%Y-%m-%d")
	echo "$v, $num, $sloc, $short_date," >> $tmp
done

mv $tmp ~/directives.csv
