---
title: systemd - How we survived Jessie and how we will break Stretch
author: Michael Biebl, Martin Pitt
date: \today
theme: Singapore
header-includes: \setbeameroption{show only notes}
...

Introduction
--------
 - Challenges with SysV → systemd in Jessie
 - Planned/potential changes for Stretch
 - How *you* can help

\note[item]{Michael/Martin intro}
\note[item]{We are the current maintainers of systemd in Debian, and I maintain it in Ubuntu.}
\note[item]{We want to review the challenges we had with the systemd migration
in Jessie, and take a look at some planned and potential changes for Stretch.
And show how you can get involved.}
\note[item]{Big thanks to Tollef Fog Heen and Michael Stapelberg, who did a
great job of maintaining systemd in the past, to Marco d'Itri as former udev
maintainer who is still around for advice, and to some contributors like
Filipe Sateler who are starting to get involved, but we always need more help.}
\note[item]{1'}

Timeline
--------

 - 2007: first discussions about modern init system (upstart)
 - 2010: first systemd Debian package
 - May 2013: Wheezy release with systemd 44 as an option
 - 2013: increasing pressure from upstreams/admins to switch to systemd
 - Oct 2013: TC bug "Which init will be the default?"
 - 2014: The "Debian Init System Wars", TC discussion
 - 2015: Jessie released with systemd 215 as default

\note[item]{\textbf{Martin}}
\note[item]{shows timeline of bringing systemd into Debian}
\note[item]{discussions about modern init system since 2007,
increasing pressure from some upstream projects and admins to use features like logind or journal)}
\note[item]{end 2013 pressure get high enough to seriously propose to switch}
\note[item]{became a Debian systemd maintainer after the start of the TC discussions, but
involved in booting in Ubuntu (upstart)}
\note[item]{1'30}

Consequences from "the" tech ctte decision
------------------------------------------

![Lennart pwned Debian](images/lennart-pwned.jpg)\

\note[item]{Debian fought for many months, some participants proclaimed end of freedom and choice}
\note[item]{kudos to tech ctte; rational, facts research}
\note[item]{switched few months before Jessie release, expected lots of broken upgrades}
\note[item]{no big complaints/rants about systemd in principle, political discussion essentially died after people realized that life goes on}
\note[item]{real work started to implement this, back to tech work}
\note[item]{Michael explains some details}
\note[item]{1'30}

---

![I will survive](images/init_wars.jpg)\


Planning the upgrade / Multiple init systems
--------------------------------------------

\note[item]{\textbf{Michael}}
\note[item]{problem: sysvinit in wheezy is an essential package}
\note[item]{support for switching between inits, not just doing a one time switch}
\note[item]{split sysvinit into sysvinit-core/sysvinit to allow upgrade in one release cycle}
\note[item]{introduction of "init" meta package to support switching init systems}
\note[item]{complexity caused by supporting multiple init systems}
\note[item]{dh-systemd / i-s-h for compatibility with non-systemd systems}
\note[item]{systemd-shim / sysv compatibility (sysvinit/sysvinit-core) as replacement for ConsoleKit}
\note[item]{syncing the state between sysv / systemd}
\note[item]{migrate configuration where sensible, don't keep using the old config files}
\note[item]{switch to systemd on upgrades for "normal" upgrade paths}
\note[item]{but keeping sysvinit around on upgrades as fallback}
\note[item]{4'30}

  - making it actually possible to install and switch between alternative inits
  - ... and doing that within one release cycle
  - additional complexity by supporting multiple inits (i-s-h, systemd-shim)
  - keeping the state in sync
  - lot's of integration work
  - migrate configuration where sensible and feasible
  - switch always but keep fallbacks


Bugs and failure reports
------------------------

![Bug Statistics](images/bugs.png)\
Source: https://qa.debian.org/data/bts/graphs/s/systemd.png

<!--
mark introduction of init meta package and switch of the default in August 2014
and the release in April 2015
-->

Bugs and failure reports
------------------------

![Bug Statistics - Release](images/bugs2.png)\
Source: https://qa.debian.org/data/bts/graphs/s/systemd.png

\note[item]{flow of new bug reports after the release was relatively manageable}
\note[item]{we follow bug reports, Michael watches user MLs; problems: tractable and fixable}
\note[item]{comparable to insserv switch: caused a lot of failures esp. with 3rd party packages with broken scripts, was also done over two releases}
\note[item]{critical bugs solved (still more corner-case bugs for two maintainers)}
\note[item]{systemd is stricter then sysvinit, sysvinit "hides" errors: nonexisting entries in fstab, dependency cycles, etc.}
\note[item]{1'30}
\note[item]{\textbf{Martin:}}
\note[item]{anecdote: set up broken encrypted swap in ecryptfs-utils}

<!--
https://qa.debian.org/popcon-graph.php?packages=sysvinit-core%2Csystemd-sysv&show_vote=on&show_recent=on&want_legend=on&want_ticks=on&from_date=&to_date=&hlght_date=&date_fmt=%25Y-%25m&beenhere=1
-->

Ongoing changes in Stretch
--------------------------
### udev: Predictable net interface naming

    /etc/udev/rules.d/70-persistent-net.rules
      eth1

→

    /{etc,lib}/systemd/network/*.link
      enp0s3
      wlp3s0

/usr/share/doc/udev/README.Debian.gz

\note[item]{\textbf{Martin:}}
\note[item]{Old: remember original random kernel name, write dynamic udev rules}
\note[item]{Inherently racy (rename1), was disabled for all VM interfaces (cloud!), r/o root, unpredictable with rolling out many identical devices}
\note[item]{New: Never use kernel name to avoid race, declarative and flexible naming policy, sensible defaults}
\note[item]{you can tweak the policy or entirely disable it to keep kernel names}
\note[item]{will take some time to get used to, but we survived fstab UUIDs and similar changes}
\note[item]{NOT for upgrades, impossible to do automatically}
\note[item]{2'}

---

### networkd

 - abstraction level similar to ifupdown
 - designed for hotplug and virtual interfaces
 - NOT a NetworkManager replacement
 - integration with resolvconf/if-up.d (?)

\note[item]{\textbf{Martin:}}
\note[item]{networkd: small/lean bringup of network interface, designed for hotplug/virtual server world}
\note[item]{config files similar to ifupdown, but no extra packages necessary for bridge/vlan/bond/etc. support}
\note[item]{NM better for desktops (D-Bus, wifi)}
\note[item]{can use it, no integration with other Debian packages yet: resolvconf, if-up.d/}
\note[item]{Over to Michael}
\note[item]{1'30}

---

### rcS.d sysvinit removal

- https://wiki.debian.org/Teams/pkg-systemd/rcSMigration
- Watch out for lintians' systemd-no-service-for-init-rcS-script

- Thanks Felipe Sateler!

\note[item]{\textbf{Michael:}}

\note[item]{Motivation}
\note[item]{support for /etc/rcS.d is a Debian specific patch which we want to get rid of}
\note[item]{rcS init scripts are the ones causing the most issues like dependency cycles}


\note[item]{mention Felipes work as an example of welcome help}

\note[item]{lintian check systemd-no-service-for-init-rcS-script
https://lintian.debian.org/tags/systemd-no-service-for-init-rcS-script.html}
\note[item]{1'}

Possible changes in Stretch
---------------------------
<!---
> - kdbus
> - presets
> - systemd-boot for EFI
-->

### kdbus
\note[item]{\textbf{Michael:}}

\note[item]{Linux kernel implementation of dbus-daemon, general purpose IPC}
\note[item]{several attempts over the years, like AF\_BUS}
\note[item]{benefits, always available, early boot, late shutdown}
\note[item]{not yet merged upstream, might be proposed (again) for 4.3?}
\note[item]{requires userspace part to setup kdbus, currently only one implementation exists with systemd}
\note[item]{won't ship a systemd version for Stretch where kdbus is mandatory}
\note[item]{can be tested in unstable/testing, requires out-of-tree module}
\note[item]{dkms module could be provided if there is interest}
\note[item]{aside from that, should work ootb if systemd is used}
\note[item]{2}

---

### presets

\note[item]{sort-of policy: enable/start services by default, different to other distros}
\note[item]{presets allow to override that "default" behaviour}
\note[item]{use "systemctl preset <foo.service>" instead of "systemctl enable <foo.service>"}
\note[item]{interesting for downstreams, derivatives, like tails or custom systems, where you don't want this behaviour}
\note[item]{problem: no support for that in update-rc.d, init-system-helpers would need that as well}
\note[item]{1'}

---

### systemd-boot
\note[item]{\textbf{Martin:}}
\note[item]{systemd-boot: small/lean boot loader for EFI for "zero conf"}
\note[item]{knows how to interpret partition type UUIDs}
\note[item]{avail since recently, but not tested much yet}
\note[item]{need to fix our installer to write correct UUIDs}
\note[item]{1'}

Potential improvements
----------------------
 - systemd in initrd
 - simplify debhelper
 - `/usr` merge
 - improve units

\note[item]{systemd in the initramfs / dracut (discussion on Monday, not clear yet)}
\note[item]{potentially also in d-i}
\note[item]{unify dh\_systemd\_{enable,start} with dh\_installinit}
\note[item]{/usr merge: not directly related to init; stateless systems, reducing combinatorial explosion}
\note[item]{tune packages to make use of more systemd features}
\note[item]{basic: SysV init script}
\note[item]{directly translate SysV to native systemd units}
\note[item]{use timers, paths, socket activation, security confinement}
\note[item]{3'}

How to help
-----------

 - work on above changes/new features
 - bugs
 - `#debian-systemd` on OFTC (mbiebl, pitti)
 - `http://lists.alioth.debian.org/mailman/listinfo/` `pkg-systemd-maintainers`
 - package in git, `git-buildpackage`
 - systemd bof on Friday, 18:00, Amsterdam

\note[item]{1'}


<!---
vim: ft=markdown
build: pandoc -t beamer -o DC15-status.pdf DC15-status.md
-->
