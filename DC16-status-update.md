---
title: systemd in Debian - a status update
author: Michael Biebl
date: \today
theme: Singapore
<!--header-includes: \setbeameroption{show only notes}-->
<!--header-includes: \setbeameroption{show notes}-->
...

introduction
--------
- polish and QA
- trimming the base OS
- getting rid of legacy cruft

polish and QA
-------------

### stable updates
- 4 stable point releases in jessie
- bug fixes and improvements
- \url{https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=pkg-systemd-maintainers@lists.alioth.debian.org;tag=jessie-backport;dist=stable}

---

### keeping systemd releasable
- elaborate unit test suite and autopkg test suite
- runs on every pull request upstream
- push patches upstream as much as possible
- allows us to track new upstream releases very closely

less is more
------------

### init
- init meta package no longer essential
- apt remove init → (buildd) chroots

### udev
- udev optional
- apt remove udev → containers

---

### package split
- systemd-{container,journal-remote,coredump}
- avoid library dependencies

---

### libsystemd-shared
- turn common code into a private shared library
- cuts the package size in half

---

### initscripts
- apt remove initscripts
- /lib/init/vars.sh → sysvinit-utils
- \url{https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=pkg-systemd-maintainers@lists.alioth.debian.org;tag=initscripts-dep}

### sysv-rc
- apt remove sysv-rc
- invoke-rc.d/update-rc.d → init-system-helpers

removing technical debt
-----------------------

### insserv-generator removal
- LSB defined facilities via /etc/insserv.conf(.d/)
- Debian specific patch
- scheduled to be removed for stretch
- \url{https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=pkg-systemd-maintainers@lists.alioth.debian.org;tag=insserv-removal}

---

### rcS init scripts removal
- early boot services
- prone to cause dependency cycles
- Debian specific patch
- scheduled to be removed for stretch
- solution: native services
- \url{https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=pkg-systemd-maintainers@lists.alioth.debian.org;tag=init-rcs-service}

adoption of systemd
-------------------
![status as of 2016-07-01](images/adoption.png)

how to help / get involved
--------------------------

 - BUGS: \url{https://bugs.debian.org/src:systemd}  
   \url{https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=pkg-systemd-maintainers@lists.alioth.debian.org}
 - IRC: `#debian-systemd` on OFTC
 - ML: \url{http://lists.alioth.debian.org/mailman/listinfo/pkg-systemd-maintainers}
 - package in git, `git-buildpackage`  
   \url{http://anonscm.debian.org/cgit/pkg-systemd/systemd.git/}

---

fix a bug and get a free hug
----------------------------
![\url{https://flic.kr/p/eiu7Ap}, CC-BY-2.0](images/hug_cats.jpg)

<!---
vim: ft=markdown
build: pandoc -t beamer -o DC16-status-update.pdf DC16-status-update.md
-->
