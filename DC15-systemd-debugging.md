---
documentclass: scrartcl
classoption: DIV30,paper=123mm:93mm,8pt
...

Your systemd tool box: dissecting and debugging boot and services
=================================================================

systemd provides a range of tools to debug boot and shutdown problems, failing
services, and optimize boot time.

 - Intro most important use cases with live demo
 - leave time for answering questions about your favourite boot related problems
 - `/usr/share/doc/systemd/README.Debian`
 - `http://freedesktop.org/wiki/Software/systemd/Debugging/`

\newpage

Scenario: clean
---------------
 - `./scen clean`, then switch to fullscreen
 - show normal boot: "quiet", reasonably fast
 - systemd centralizes all logging into the journal ("log db")
 - show journal modes:
    * full: `journalctl`
    * much like syslog, but contains dmesg, service/daemon stdout/err
    * per-unit: `journalctl -u dbus`
    * per-prio: `journalctl -p warning`
    * matches: `journalctl _UID=1000`
    * current boot: `journalctl -b`
    * previous boot: `journalctl -b -1` (needs persistant journal)
    * `journalctl --list-boots`
 - loads of options, can be combined
 - dynamic rotation, uses max % of avail RAM/disk (configurable, percent,
   absolute, `journald.conf`)
 - can boot with "debug" to get truckloads of output from kernel and systemd
 - dynamically change global debug level:
    * start second terminal with `journalctl -f`
    * `sudo systemd-analyze set-log-level debug`
    * `sudo systemctl restart cron.service`

Now show some typical problems and how to debug them

\newpage

Scenario: broken early-boot unit hangs boot
-------------------------------------------

Prep for demo: `/etc/systemd/system.conf` `DefaultTimeout*=10s`

 - `./scen hang`, then switch to fullscreen
 * Early-boot-blocker hanging job
 * shows only errors on "quiet" after timeout; early boot → no getty
 * Ctrl+Alt+1, `system_reset`
 * boot without quiet and with systemd.debug-shell
 * Alt+F9 to get to debug shell; "always" there
 * `systemctl list-jobs` → running `blocker.service`
 * `journalctl -e`
 * `systemctl status -l blocker.service`
   → shows status, process tree, pids, and recent journal output
 * `systemctl cat blocker.service`
 * fix: `vi /lib/systemd/system/blocker.service`
 * `reboot -f`

\newpage

Scenario: broken fstab
----------------------
 - `./scen clean`, then switch to fullscreen
 - change UUID for `/var` in `/etc/fstab`, `/sbin/reboot`
 - wait for timeout, point to it
 - won't let you boot with missing devices from fstab (unlike sysv)
 - throws you into emergency shell (root pwd `a`)
 - explain `nofail`: fix UUID of `/var`, break it for `/usr/local`, add `nofail`
 - show `journalctl -p err`

\newpage

Scenario: boot deadlock
-----------------------
 - `./scen dead`, switch to fullscreen
 - hangs, switch to debug shell
 - `systemctl list-jobs`
 - `systemctl status -l networking` → hanging `systemctl` process
 - kill systemctl
 - fix: add `--no-block` to `/etc/network/if-up.d/openssh-server`
 - early boot transition, systemctl blocks for completion by default

\newpage

Scenario: shutdown hang
-----------------------
 - `./scen down`, switch to fullscreen
 - `systemctl start debug-shell`, reboot
 - `systemctl list-jobs`
 - edit `/lib/systemd/system/netstuff.target`, add `After=network.target`,
   remove `Before=systemd-random-seed.service`
 - systemctl halt!
 - symlink /usr/lib/systemd/system-shutdown -> /lib/systemd/system-shutdown

\newpage

Scenario: slow boot
-------------------
 - `./scen slow`, then switch to fullscreen
 - boot takes ~ 5 s longer
   * `systemd-analyze`: summary; quite long
   * `systemd-analyze critical-chain`
   * `systemd-analyze blame`
   * `systemd-analyze plot > /tmp/s.svg; viewnior /tmp/s.svg`
   * expensive.service (at bottom) is holding up multi-user.target and lxdm
   * poor man's bootchart, always available

Scenario: slowbc
----------------
Same, but with bootchart

 - `./scen slowbc`, with `init=/lib/systemd/systemd-bootchart`
 - `viewnior /run/log/*.svg`
 - Debian kernel is missing `CONFIG_SCHEDSTAT`; using Ubuntu kernel

\newpage

Scenario: serial console
------------------------
 - `./scen slowbc`, with `console=ttyS0`
 - systemd reads `console=` (can be repeated) and auto-launches gettys

Debugging units
---------------
- systemd-nspawn for debugging/testing unit files (as alternative to VMs and
  containers):
   - `schroot -c jessie -u root`
   - `apt install dbus`
   - `sudo systemd-nspawn -D /tmp/jessie-amd64* -b`

- `systemd-analyze verify foo.service` → unknown keywords, syntactic errors, etc.

<!--
 - debugging suspend problems (no pm-utils anymore!)
   - suspend hooks
-->
